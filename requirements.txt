# development
flake8==3.8.4
flake8-isort==4.0.0
black==20.8b1
pytest==7.1.2
# for json_cfdi
xmlschema>=1.11.1
jsonpickle>=2.0